package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/object"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

const (
	terraformImagesURL = "git@gitlab.com:gitlab-org/terraform-images.git"
)

var (
	authorName  = flag.String("author", "", "Name for the commit author")
	authorEmail = flag.String("email", "", "Email for the commit author")
)

func init() {
	flag.Parse()

	if *authorEmail == "" || *authorName == "" {
		fmt.Println("Usage: tiu -author <name> -email <mail>")
		os.Exit(1)
	}
}

func main() {
	v, err := getLatestTerraformVersion()

	version := v
	if strings.HasPrefix(v, "v") {
		version = strings.TrimPrefix(v, "v")
	}

	err = updateRepo(version, &object.Signature{
		Name:  *authorName,
		Email: *authorEmail,
		When:  time.Now(),
	})
	if err != nil {
		log.Fatal(err)
	}
}

func getLatestTerraformVersion() (string, error) {

	resp, err := http.Get("https://api.github.com/repos/hashicorp/terraform/releases/latest")
	if err != nil {
		return "", err
	}

	var response struct {
		TagName string `json:"tag_name"`
	}
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		return "", err
	}

	return response.TagName, nil
}

func updateRepo(version string, author *object.Signature) error {
	tmpDir, err := os.MkdirTemp("", "tiu-")
	if err != nil {
		return err
	}

	r, err := git.PlainClone(tmpDir, false, &git.CloneOptions{
		URL: terraformImagesURL,
	})

	if err != nil {
		return err
	}

	ciFilePath := path.Join(tmpDir, ".gitlab-ci.yml")
	ciYamlBytes, err := os.ReadFile(ciFilePath)
	if err != nil {
		return err
	}

	lines := strings.Split(string(ciYamlBytes), "\n")
	for idx, line := range lines {
		if strings.Contains(line, "TERRAFORM_BINARY_VERSION: \"1.") {
			lines[idx] = "      - TERRAFORM_BINARY_VERSION: \"" + version + "\""
		}
	}

	updatedCiYaml := strings.Join(lines, "\n")
	err = ioutil.WriteFile(ciFilePath, []byte(updatedCiYaml), 0644)
	if err != nil {
		return err
	}

	w, err := r.Worktree()
	if err != nil {
		return err
	}

	_, err = w.Commit("feat: Updated terraform to "+version, &git.CommitOptions{
		All:    true,
		Author: author,
	})
	if err != nil {
		return err
	}

	return nil
}
