# Terraform-images updater

Just a way to update the terraform version in [terraform-images](https://gitlab.com/gitlab-org/terraform-images).

Needs lots of polish and is overall naive but lets you write:

`tiu -author <your-name> -email <you-mail>`

To get a new commit like

```shell
commit e9b6ea2b4ba91a9f6238f9cf0ceaec94a4b95f1b (HEAD -> master)
Author: Philipp Böschen <gitlab@phil.deichindianer.de>
Date:   Fri Mar 18 13:37:39 2022 +0100

    feat: Updated terraform to 1.1.7
```

Does not push yet and stores the thing in a local tmp dir but that can be fiddled with.